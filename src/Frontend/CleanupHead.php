<?php namespace Mezhik91WP\Frontend;

use Mezhik91WP\Mezhik91WPModule;
use Mezhik91WP\Exceptions\FunctionDoesnNotExist;

class CleanupHead implements Mezhik91WPModule {

    private $remove_scripts;

    const REMOVE_BY_DEFAUL = [
        array('wp_head', 'wlwmanifest_link'),
        array('wp_head', 'rsd_link'),
        array('wp_head', 'print_emoji_detection_script', 7),
        array('wp_print_styles', 'print_emoji_styles'),
        array('wp_head', 'wp_generator'),
        array('wp_head', 'wp_resource_hints', 2),
        array('wp_head', 'rest_output_link_wp_head'),
        array('wp_head', 'wp_oembed_add_discovery_links'),
        array('template_redirect', 'rest_output_link_header', 11)
    ];

    function __construct($remove_from_head = []) {
        if ( count($remove_from_head) > 0) {
            $this->remove_scripts = $remove_from_head;
        } else {
            $this->remove_scripts = self::REMOVE_BY_DEFAUL;
        }
    }

    function init () {
        $this->remove_from_head();
    }

    private function remove_from_head() {
        if ( ! function_exists('remove_action')) {
            throw new FunctionDoesnNotExist('Such Wordpress action doesn\'t exists');
        }
        foreach($this->remove_scripts as $script) {
            remove_action($script[0], $script[1], $script[2]);
        }
    }
}
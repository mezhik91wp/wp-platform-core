<?php namespace Mezhik91WP;

class Init {

    static $isLoaded = false;
    private $modules = [];

    function __construct($modules) {
        if ( ! self::$isLoaded ) {
            $this->modules = $modules;
        }
        self::$isLoaded = true;
        $this->load_modules();
    }

    private function load_modules() {
        foreach($this->modules as $module) {
            $module->init();
        }
    }
}